using System.Collections.Generic;
using MusicalNotes.UI;
using OpenCvSharp;
using TMPro;
using UnityEngine;
using Unity.Barracuda;


namespace MusicalNotes
{
    public class NoteClassifier : MonoBehaviour
    {
        [Header("UI Dependencies")] 
        [SerializeField] private Transform _gridRoot;

        [Header("Data")]
        [SerializeField] private Texture2D[] _testImages;
        [SerializeField] private NNModel _modelAsset;
        [SerializeField] private PredictedNoteInfoCard _infoCardPrefab;
        
        private Model _runtimeModel;
        private IWorker _worker;

        public static readonly Dictionary<int, string> LabelMap = new()
        {
            {0, "Half"},
            {1, "Quarter"},
            {2, "Whole"}
        };

        private void Start()
        {
            _runtimeModel = ModelLoader.Load(_modelAsset);
            _worker = WorkerFactory.CreateWorker(WorkerFactory.Type.ComputePrecompiled, _runtimeModel);
            
            //ClassifyTestImages();
        }

        public int ClassifyImage(Texture2D image)
        {
            Tensor input = new Tensor(image, 1);

            _worker.Execute(input);

            Tensor output = _worker.PeekOutput();

            var predictedLabel = output.ArgMax()[0];
            
            Debug.Log(LabelMap[predictedLabel]);

            input.Dispose();
            output.Dispose();
            
            return predictedLabel;
        }
        
        private void ClassifyTestImages()
        {
            foreach (var image in _testImages)
            {
                Tensor input = new Tensor(image, 1);

                _worker.Execute(input);

                Tensor output = _worker.PeekOutput();

                var predictedLabel = output.ArgMax()[0];
                Debug.Log(predictedLabel);

                var infoCard = Instantiate(_infoCardPrefab, _gridRoot, true);
                infoCard.Configure(image, image.name, LabelMap[predictedLabel]);
                
                var imgMat = new Mat(image.height, image.width, MatType.CV_8S);
                //Mat.FromImageData()
                input.Dispose();
                output.Dispose();
            }
        }
        
        private void OnDestroy()
        {
            _worker.Dispose();
        }
    }
}