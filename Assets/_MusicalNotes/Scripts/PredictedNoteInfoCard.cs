using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MusicalNotes.UI
{
    public class PredictedNoteInfoCard : MonoBehaviour
    {
        [SerializeField] private Image _noteImage;
        [SerializeField] private TextMeshProUGUI _fileName;
        [SerializeField] private TextMeshProUGUI _predictedClass;

        public void Configure(Texture2D image, string fileName, string predictedClass)
        {
            var rect = new Rect(0, 0, image.width, image.height);
            _noteImage.sprite = Sprite.Create(image, rect, new Vector2(0.5f, 0.5f), 100);

            _fileName.text = fileName;
            _predictedClass.text = predictedClass;
        }
    }
}