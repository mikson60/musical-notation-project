using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

// using OpenCvSharp;

namespace MusicalNotes
{
    public class StaffController : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private RectTransform _staffRect;
        [SerializeField] private RectTransform _drawingAreaParent;
        [SerializeField] private GameObject _drawingArea;
        [SerializeField] private MouseDraw _mouseDraw;
        [SerializeField] private NoteClassifier _classifier;
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private TextMeshProUGUI _lastClassifiedNote;

        [SerializeField] private RawImage _test;

        [Header("Music Notes SFX")] 
        [SerializeField] private List<AudioClip> _quarterNotes;
        [SerializeField] private List<AudioClip> _halfNotes;
        [SerializeField] private List<AudioClip> _wholeNotes;
        
        private int _currentSection = -1;
        private int _posInSection = 0;

        private Dictionary<int, GameObject> _drawnImages = new();
        private List<NoteClassAndPitchPair> _musicQueue = new();

        private Coroutine _playNotesRoutine;
        
        private static readonly Dictionary<int, string> NoteMap = new()
        {
            {0, "C4"},
            {1, "D4"},
            {2, "E4"},
            {3, "F4"},
            {4, "G4"},
            {5, "A4"},
            {6, "B4"},
            {7, "C5"},
            {8, "D5"},
            {9, "E5"},
            {10, "F5"},
            {11, "G5"},
            {12, "A5"}
        };
        
        private void Start()
        {
            PlaceDrawingAreaInSection(_currentSection, _posInSection);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Return))
                StepForward();
            else if (Input.GetKeyDown(KeyCode.Space))
                PlayNotes();
            else if (Input.GetKeyDown(KeyCode.Backspace))
                RemoveLastNote();
        }

        private void PlaceDrawingAreaInSection(int section, int posInSection)
        {
            var staffRectAnchoredPosition = _staffRect.anchoredPosition;
            var sectionMidPosX = staffRectAnchoredPosition.x + 340f * section;
            var startPos = sectionMidPosX - 130f;
            var drawingAreaPosition = startPos + 85f * posInSection;
            
            _drawingAreaParent.anchoredPosition = new Vector2(drawingAreaPosition, 0f);
        }
        
        private void StepForward()
        {
            var processedDrawnImage = PreprocessForClassification(_drawingArea.GetComponent<RawImage>().texture);
            var noteHeadCenter = GetNoteHeadPosition(processedDrawnImage);
            Debug.Log($"Note head center {noteHeadCenter}");
            
            int centerRadius = 5;
            Color centerColor = Color.green;
            // Color the center pixels green
            for (int x = (int)(noteHeadCenter.x - centerRadius); x <= noteHeadCenter.x + centerRadius; x++)
            {
                for (int y = (int)(noteHeadCenter.y - centerRadius); y <= noteHeadCenter.y + centerRadius; y++)
                {
                    if (x >= 0 && x < processedDrawnImage.width && y >= 0 && y < processedDrawnImage.height && Vector2.Distance(noteHeadCenter, new Vector2(x, y)) <= centerRadius)
                    {
                        processedDrawnImage.SetPixel(x, y, centerColor);
                    }
                }
            }
            
            // Display the image that goes into the model.
            _test.texture = processedDrawnImage;
            
            // Export png for debugging.
            /*
            byte[] textureData = processedDrawnImage.EncodeToPNG();
            string filePath = Application.dataPath + "/exportedTexture.png"; // Define the file path where the PNG will be saved
            File.WriteAllBytes(filePath, textureData);
            Debug.Log("Texture exported as PNG: " + filePath);
            */
            
            // Classify the symbol.
            var noteClass = _classifier.ClassifyImage(processedDrawnImage);
            CopyDrawnImage();

            var pitchIndex = GetPitchIndex(noteHeadCenter.y);
            var noteClassAndPitchPair = new NoteClassAndPitchPair(noteClass, pitchIndex);
            _musicQueue.Add(noteClassAndPitchPair);
            if (!_audioSource.isPlaying)
            {
                PlayNote(noteClassAndPitchPair);
            }

            _lastClassifiedNote.text = $"{NoteClassifier.LabelMap[noteClass]} {NoteMap[pitchIndex]}";
            
            var steps = NoteClassToSteps(1);
            
            _posInSection += steps;
            if (_posInSection > 3)
            {
                _posInSection = 0;
                _currentSection++;
            }

            PlaceDrawingAreaInSection(_currentSection, _posInSection);
        }

        private void RemoveLastNote()
        {
            if (_currentSection == -1 && _posInSection == 0)
            {
                Debug.Log("No more to remove.");
                return;
            }

            if (_posInSection == 0)
            {
                _currentSection--;
                _posInSection = 3;
            }
            else
            {
                _posInSection--;
            }
            
            if (_musicQueue.Count > 0)
                _musicQueue.RemoveAt(_musicQueue.Count-1);
            
            var index = 4*(_currentSection+1) + _posInSection;
            if (_drawnImages.TryGetValue(index, out var drawnImage))
            {
                Destroy(drawnImage);
                _drawnImages.Remove(index);
            }
            
            PlaceDrawingAreaInSection(_currentSection, _posInSection);
        }

        private int NoteClassToSteps(int noteClass) => noteClass switch
        {
            0 => 2,
            1 => 1,
            2 => 4
        };

        private Texture2D PreprocessForClassification(Texture texture)
        {
            // Create a new white Texture2D with the same dimensions as the RawImage texture
            var convertedTexture = new Texture2D(texture.width, texture.height);
            Color32[] pixels = new Color32[convertedTexture.width * convertedTexture.height];

            // Fill the new texture with white color
            for (int i = 0; i < pixels.Length; i++)
            {
                pixels[i] = Color.white;
            }

            convertedTexture.SetPixels32(pixels);
            convertedTexture.Apply();

            // Get the pixels from the RawImage texture
            Texture2D rawTexture = texture as Texture2D;
            Color32[] rawPixels = rawTexture.GetPixels32();

            // Copy the pixels from the RawImage texture onto the new white texture,
            // preserving the transparent areas and replacing black with white
            for (int i = 0; i < rawPixels.Length; i++)
            {
                if (rawPixels[i].a > 0)
                {
                    convertedTexture.SetPixel(i % convertedTexture.width, i / convertedTexture.width, rawPixels[i]);
                    continue;
                }
            }

            convertedTexture.Apply();
            return convertedTexture;
        }
        
        private void CopyDrawnImage()
        {
            var drawnImage = Instantiate(_drawingArea, _staffRect, true);
            Destroy(drawnImage.GetComponent<MouseDraw>());

            var index = 4*(_currentSection+1) + _posInSection;
            _drawnImages.Add(index, drawnImage);
            
            _mouseDraw.Init();
        }
        
        private Vector2 GetNoteHeadPosition(Texture2D texture)
        {
            Color noteColor = Color.black;
            int windowSize = 8;

            // Get all pixel data on the main thread
            Color[] pixelData = texture.GetPixels();
            int width = texture.width;
            int height = texture.height;

            int maxDensity = 0;
            Vector2 noteCenter = Vector2.zero;
            object lockObject = new object();

            Parallel.For(0, width - windowSize, x =>
            {
                for (int y = 0; y <= height - windowSize; y++)
                {
                    int density = 0;
                    for (int wx = x; wx < x + windowSize; wx++)
                    {
                        for (int wy = y; wy < y + windowSize; wy++)
                        {
                            if (pixelData[wy * width + wx] == noteColor)
                            {
                                density++;
                            }
                        }
                    }

                    lock (lockObject)
                    {
                        if (density > maxDensity)
                        {
                            maxDensity = density;
                            noteCenter = new Vector2(x + windowSize / 2, y + windowSize / 2);
                        }
                    }
                }
            });

            return noteCenter;
        }

        private int GetPitchIndex(float y)
        {
            return y switch
            {
                < 14 => 0,
                >= 14 and <= 24 => 1,
                >= 24 and <= 40 => 2,
                >= 40 and <= 56 => 3,
                >= 56 and <= 72 => 4,
                >= 72 and <= 88 => 5,
                >= 88 and <= 104 => 6,
                >= 104 and <= 120 => 7,
                >= 120 and <= 136 => 8,
                >= 136 and <= 152 => 9,
                >= 152 and <= 168 => 10,
                >= 168 and <= 184 => 11,
                >= 176 and <= 200 => 12,
                _ => -1
            };
        }

        private void PlayNotes()
        {
            if (_playNotesRoutine != null)
            {
                StopCoroutine(_playNotesRoutine);
                _playNotesRoutine = null;
            }

            _playNotesRoutine = StartCoroutine(PlayNotesRoutine());
        }

        private IEnumerator PlayNotesRoutine()
        {
            _audioSource.Stop();
            var currentNoteIndex = 0;

            while (currentNoteIndex < _musicQueue.Count)
            {
                Debug.Log(_musicQueue.Count);
                var pair = _musicQueue[currentNoteIndex];
                currentNoteIndex++;
                
                PlayNote(pair);

                var secondsToWait = pair.Class switch
                {
                    0 => 1f,
                    1 => 0.6f,
                    2 => 2f,
                    _ => 0f
                };
                
                yield return new WaitForSeconds(secondsToWait);
            }
        }

        private void PlayNote(NoteClassAndPitchPair pair)
        {
            var noteClass = pair.Class;
            var pitchIndex = pair.Pitch; 
            
            if (noteClass == 0) // half
            {
                _audioSource.PlayOneShot(_halfNotes[pitchIndex]);
            }
            else if (noteClass == 1) // quarter
            {
                _audioSource.PlayOneShot(_quarterNotes[pitchIndex]);
            }
            else if (noteClass == 2) // whole
            {
                _audioSource.PlayOneShot(_wholeNotes[pitchIndex]);
            }
        }

        private struct NoteClassAndPitchPair
        {
            public int Class;
            public int Pitch;

            public NoteClassAndPitchPair(int @class, int pitch)
            {
                Class = @class;
                Pitch = pitch;
            }
        }
        
        /*private Vector2 GetNoteHeadPosOpenCV(Texture2D texture)
        {
            var mat = OpenCvSharp.Unity.TextureToMat(texture);

            // Convert image to grayscale
            Mat gray = new Mat();
            Cv2.CvtColor(mat, gray, ColorConversionCodes.BGR2GRAY);

// Reduce noise
            Mat blurred = new Mat();
            Cv2.GaussianBlur(gray, blurred, new OpenCvSharp.Size(5, 5), 0);

// Threshold the image
            Mat binary = new Mat();
            Cv2.Threshold(blurred, binary, 0, 255, ThresholdTypes.BinaryInv | ThresholdTypes.Otsu);

// Perform morphological opening to remove small black objects
            Mat morphed = new Mat();
            Cv2.MorphologyEx(binary, morphed, MorphTypes.Open, null);

// Find contours in the image
            OpenCvSharp.Point[][] contours;
            HierarchyIndex[] hierarchy;
            Cv2.FindContours(morphed, out contours, out hierarchy, RetrievalModes.External, ContourApproximationModes.ApproxSimple);

// Find the contour with the largest area that is roughly circular
            double maxArea = 0;
            OpenCvSharp.Point[] maxContour = null;
            foreach (OpenCvSharp.Point[] contour in contours)
            {
                double area = Cv2.ContourArea(contour);
                if (area > maxArea)
                {
                    maxArea = area;
                    maxContour = contour;
                }
            }

            if (maxContour != null)
            {
                // Filter based on area and aspect ratio
                OpenCvSharp.Rect boundingRect = Cv2.BoundingRect(maxContour);
                double centerX = boundingRect.X + boundingRect.Width / 2;
                double centerY = boundingRect.Y + boundingRect.Height / 2;

                return new Vector2((float)centerX, (float)centerY);
                // centerX and centerY are the coordinates of the note head center
            }
            Debug.Log("NO MAX CONTOUR");

            return GetNoteHeadPosition(texture);
        }*/
    }
}